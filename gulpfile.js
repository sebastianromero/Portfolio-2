//'use strict';
var gulp = require('gulp');
var browserSync = require('browser-sync');
var coffee = require('gulp-coffee');//dependencia de gulp-notify
var concat = require('gulp-concat');
var filter = require('gulp-filter');
var sass = require('gulp-sass');
var gutil = require('gulp-util');
var minifyCss = require('gulp-minify-css');
var notify = require("gulp-notify");
var plumber = require('gulp-plumber');
var prefix = require('gulp-autoprefixer');
var reload = browserSync.reload;
var replace = require('gulp-replace');
var uglify = require('gulp-uglify');

var dir= '192.168.1.4/portfolio';


//Sass
gulp.task('scss', function () {
    gulp.src(['sass/style.scss'])

        .pipe(plumber())
        .pipe(sass().on('err', onError))
        .pipe(prefix("last 4 versions",
            "> 5%",
            "ie 9", "ie 10",
              "ie 8",
            "ie 7",
            "ios 4"))
        .pipe(minifyCss())
        .pipe(replace('@charset "UTF-8";',''))
        .pipe(filter('**/*.css'))
        .pipe(gulp.dest('./'))
        .pipe(browserSync.reload({stream: true}))
});

//Javascript
gulp.task('js', function () {
    gulp.src(['js/src/_*.js'])
        .pipe(plumber())
        .pipe(concat('scripts.js'))
        .pipe(uglify())
        .pipe(gulp.dest('js'))
        .pipe(browserSync.reload({stream: true}));
});


//Uglify
gulp.task('otros_scripts', function () {
    gulp.src(['js/src/*.js', '!js/src/_*.js'])
        .pipe(uglify())
        .pipe(gulp.dest('js'))
        .pipe(browserSync.reload({stream: true}));
});


//Browsersync
gulp.task('browser-sync', function () {
    browserSync.init({
        tunnel: "portfolio",
        proxy: dir,
        online: false,
        notify: false
    });
});


gulp.task('watch', function () {
    gulp.watch(['sass/**/*.scss', 'sass/*.scss'], ['scss'], reload);
    gulp.watch(['*.php',  'paginas/*.php'], reload);
    gulp.watch(['js/src/*.js'], ['js'], reload);

});






var onError = function(err) {
    gutil.beep();
    sass.logError;
};




gulp.task('default', ['scss','js', 'otros_scripts', 'watch', 'browser-sync'], function () {
});
