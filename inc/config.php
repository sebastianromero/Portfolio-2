<?php
remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
remove_action( 'wp_print_styles', 'print_emoji_styles' );


/**
 * Source: http://nicolasgallagher.com/anatomy-of-an-html5-wordpress-theme/
 */

// Remove links to the extra feeds (e.g. category feeds)
remove_action( 'wp_head', 'feed_links_extra', 3 );
// Remove links to the general feeds (e.g. posts and comments)
remove_action( 'wp_head', 'feed_links', 2 );
// Remove link to the RSD service endpoint, EditURI link
remove_action( 'wp_head', 'rsd_link' );
// Remove link to the Windows Live Writer manifest file
remove_action( 'wp_head', 'wlwmanifest_link' );
// Remove index link
remove_action( 'wp_head', 'index_rel_link' );
// Remove prev link
remove_action( 'wp_head', 'parent_post_rel_link', 10, 0 );
// Remove start link
remove_action( 'wp_head', 'start_post_rel_link', 10, 0 );
// Display relational links for adjacent posts
remove_action( 'wp_head', 'adjacent_posts_rel_link', 10, 0 );
// Remove XHTML generator showing WP version
remove_action( 'wp_head', 'wp_generator' );













/**
 * Agregar los posteos y las taxonomias
 */

// Register Custom Post Type
function blog() {

    $labels = array(
        'name'                => _x( 'Posts', 'Post Type General Name', 'portfolio' ),
        'singular_name'       => _x( 'Post', 'Post Type Singular Name', 'portfolio' ),
        'menu_name'           => __( 'Blog', 'portfolio' ),
        'name_admin_bar'      => __( 'Posteo del blog', 'portfolio' ),
        'parent_item_colon'   => __( 'Parent Item:', 'portfolio' ),
        'all_items'           => __( 'All Items', 'portfolio' ),
        'add_new_item'        => __( 'Add New Item', 'portfolio' ),
        'add_new'             => __( 'Add New', 'portfolio' ),
        'new_item'            => __( 'New Item', 'portfolio' ),
        'edit_item'           => __( 'Edit Item', 'portfolio' ),
        'update_item'         => __( 'Update Item', 'portfolio' ),
        'view_item'           => __( 'View Item', 'portfolio' ),
        'search_items'        => __( 'Search Item', 'portfolio' ),
        'not_found'           => __( 'Not found', 'portfolio' ),
        'not_found_in_trash'  => __( 'Not found in Trash', 'portfolio' ),
    );
    $args = array(
        'label'               => __( 'blog', 'portfolio' ),
        'description'         => __( 'Blog', 'portfolio' ),
        'labels'              => $labels,
        'supports'            => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail', 'comments', 'trackbacks', 'revisions', 'custom-fields', 'page-attributes', 'post-formats', ),
        'taxonomies'          => array( 'categorias_blog' ),
        'hierarchical'        => false,
        'public'              => true,
        'show_ui'             => true,
        'show_in_menu'        => true,
        'menu_position'       => 5,
        'menu_icon'           => 'dashicons-lightbulb',
        'show_in_admin_bar'   => true,
        'show_in_nav_menus'   => true,
        'can_export'          => true,
        'has_archive'         => true,
        'exclude_from_search' => false,
        'publicly_queryable'  => true,
        'capability_type'     => 'post',
    );
    register_post_type( 'blog', $args );

}

// Hook into the 'init' action
add_action( 'init', 'blog', 0 );


// Register Custom Taxonomy
function categorias_blog() {

    $labels = array(
        'name'                       => 'Temas',
        'singular_name'              => 'Tema',
        'menu_name'                  => 'Temas',
        'all_items'                  => 'All Items',
        'parent_item'                => 'Parent Item',
        'parent_item_colon'          => 'Parent Item:',
        'new_item_name'              => 'New Item Name',
        'add_new_item'               => 'Add New Item',
        'edit_item'                  => 'Edit Item',
        'update_item'                => 'Update Item',
        'separate_items_with_commas' => 'Separate items with commas',
        'search_items'               => 'Search Items',
        'add_or_remove_items'        => 'Add or remove items',
        'choose_from_most_used'      => 'Choose from the most used items',
        'not_found'                  => 'Not Found',
    );
    $args = array(
        'labels'                     => $labels,
        'hierarchical'               => true,
        'public'                     => true,
        'show_ui'                    => true,
        'show_admin_column'          => true,
        'show_in_nav_menus'          => true,
        'show_tagcloud'              => true,
    );
    register_taxonomy( 'tema', array( 'blog' ), $args );

}

// Hook into the 'init' action
add_action( 'init', 'categorias_blog', 0 );





// Register Custom Taxonomy
function clientes() {

    $labels = array(
        'name'                       => 'Clientes',
        'singular_name'              => 'Cliente',
        'menu_name'                  => 'Clientes',
        'all_items'                  => 'Todos los clientes',
        'parent_item'                => 'Parent Item',
        'parent_item_colon'          => 'Parent Item:',
        'new_item_name'              => 'New Item Name',
        'add_new_item'               => 'Agregar nuevo cliente',
        'edit_item'                  => 'Editar cliente',
        'update_item'                => 'Actualizar cliente',
        'view_item'                  => 'Ver cliente',
        'separate_items_with_commas' => 'Separar los clientes con coma',
        'add_or_remove_items'        => 'Agregar o borrar clientes',
        'choose_from_most_used'      => 'Elegir de los clientes más usados',
        'popular_items'              => 'Clientes populares',
        'search_items'               => 'Buscar clientes',
        'not_found'                  => 'Cliente no encontrado',
    );
    $args = array(
        'labels'                     => $labels,
        'hierarchical'               => false,
        'public'                     => true,
        'show_ui'                    => true,
        'show_admin_column'          => true,
        'show_in_nav_menus'          => true,
        'show_tagcloud'              => true,
    );
    register_taxonomy( 'cliente', array( 'post' ), $args );

}

// Hook into the 'init' action
add_action( 'init', 'clientes', 0 );






// Register Custom Taxonomy
function servicios() {

    $labels = array(
        'name'                       => 'Servicios',
        'singular_name'              => 'Servicio',
        'menu_name'                  => 'Servicios',
        'all_items'                  => 'All Items',
        'parent_item'                => 'Parent Item',
        'parent_item_colon'          => 'Parent Item:',
        'new_item_name'              => 'New Item Name',
        'add_new_item'               => 'Add New Item',
        'edit_item'                  => 'Edit Item',
        'update_item'                => 'Update Item',
        'view_item'                  => 'View Item',
        'separate_items_with_commas' => 'Separate items with commas',
        'add_or_remove_items'        => 'Add or remove items',
        'choose_from_most_used'      => 'Choose from the most used items',
        'popular_items'              => 'Popular Items',
        'search_items'               => 'Search Items',
        'not_found'                  => 'Not Found',
    );
    $args = array(
        'labels'                     => $labels,
        'hierarchical'               => false,
        'public'                     => true,
        'show_ui'                    => true,
        'show_admin_column'          => true,
        'show_in_nav_menus'          => true,
        'show_tagcloud'              => true,
    );
    register_taxonomy( 'servicios', array( 'post' ), $args );

}

// Hook into the 'init' action
add_action( 'init', 'servicios', 0 );


// Register Custom Taxonomy
function colaboradores() {

    $labels = array(
        'name'                       => 'Colaboradores',
        'singular_name'              => 'Colaborador',
        'menu_name'                  => 'Colaboradores',
        'all_items'                  => 'Todos los colaboradores',
        'parent_item'                => 'Parent Item',
        'parent_item_colon'          => 'Parent Item:',
        'new_item_name'              => 'Nuevo colaborador',
        'add_new_item'               => 'Agregar colaborador',
        'edit_item'                  => 'Editar colaborador',
        'update_item'                => 'Actualizar colaborador',
        'view_item'                  => 'Ver colaborador',
        'separate_items_with_commas' => 'Separar los colaboradores con coma',
        'add_or_remove_items'        => 'Agregar o quitar colaboradores',
        'choose_from_most_used'      => 'Elegir de los más usados',
        'popular_items'              => 'Colaboradores populares',
        'search_items'               => 'Buscar colaboradores',
        'not_found'                  => 'No encontrado',
    );
    $args = array(
        'labels'                     => $labels,
        'hierarchical'               => false,
        'public'                     => true,
        'show_ui'                    => true,
        'show_admin_column'          => true,
        'show_in_nav_menus'          => true,
        'show_tagcloud'              => true,
    );
    register_taxonomy( 'colaborador', array( 'post' ), $args );

}

// Hook into the 'init' action
add_action( 'init', 'colaboradores', 0 );


// Register Custom Taxonomy
function tecnología() {

    $labels = array(
        'name'                       => 'Tecnologias',
        'singular_name'              => 'Tecnologia',
        'menu_name'                  => 'Tecnologías',
        'all_items'                  => 'Todas las tecnologías',
        'parent_item'                => 'Parent Item',
        'parent_item_colon'          => 'Parent Item:',
        'new_item_name'              => 'Nueva tecnología',
        'add_new_item'               => 'Agregar nueva tecnología',
        'edit_item'                  => 'Editar tecnología',
        'update_item'                => 'Actualizar tecnología',
        'view_item'                  => 'Ver tecnología',
        'separate_items_with_commas' => 'Separar las tecnologías con coma',
        'add_or_remove_items'        => 'Agregar o quitar tecnologías',
        'choose_from_most_used'      => 'Elegir de la más usadas',
        'popular_items'              => 'Tecnologías populares',
        'search_items'               => 'Buscar tecnología',
        'not_found'                  => 'Tecnología no encontrada',
    );
    $args = array(
        'labels'                     => $labels,
        'hierarchical'               => false,
        'public'                     => true,
        'show_ui'                    => true,
        'show_admin_column'          => true,
        'show_in_nav_menus'          => true,
        'show_tagcloud'              => true,
    );
    register_taxonomy( 'tecnologia', array( 'post' ), $args );

}

// Hook into the 'init' action
add_action( 'init', 'tecnología', 0 );





// Custom filter function to modify default gallery shortcode output
// Source: http://robido.com/wordpress/wordpress-gallery-filter-to-modify-the-html-output-of-the-default-gallery-shortcode-and-style/
function my_post_gallery( $output, $attr ) {

    // Initialize
    global $post, $wp_locale;

    // Gallery instance counter
    static $instance = 0;
    $instance++;

    // Validate the author's orderby attribute
    if ( isset( $attr['orderby'] ) ) {
        $attr['orderby'] = sanitize_sql_orderby( $attr['orderby'] );
        if ( ! $attr['orderby'] ) unset( $attr['orderby'] );
    }

    // Get attributes from shortcode
    extract( shortcode_atts( array(
        'order'      => 'ASC',
        'orderby'    => 'menu_order ID',
        'id'         => $post->ID,
        'itemtag'    => 'li',
        'icontag'    => 'figure',
        'captiontag' => 'figcaption',
        'columns'    => 3,
        'size'       => 'thumbnail',
        'include'    => '',
        'exclude'    => ''
    ), $attr ) );

    // Initialize
    $id = intval( $id );
    $attachments = array();
    if ( $order == 'RAND' ) $orderby = 'none';

    if ( ! empty( $include ) ) {

        // Include attribute is present
        $include = preg_replace( '/[^0-9,]+/', '', $include );
        $_attachments = get_posts( array( 'include' => $include, 'post_status' => 'inherit', 'post_type' => 'attachment', 'post_mime_type' => 'image', 'order' => $order, 'orderby' => $orderby ) );

        // Setup attachments array
        foreach ( $_attachments as $key => $val ) {
            $attachments[ $val->ID ] = $_attachments[ $key ];
        }

    } else if ( ! empty( $exclude ) ) {

        // Exclude attribute is present
        $exclude = preg_replace( '/[^0-9,]+/', '', $exclude );

        // Setup attachments array
        $attachments = get_children( array( 'post_parent' => $id, 'exclude' => $exclude, 'post_status' => 'inherit', 'post_type' => 'attachment', 'post_mime_type' => 'image', 'order' => $order, 'orderby' => $orderby ) );
    } else {
        // Setup attachments array
        $attachments = get_children( array( 'post_parent' => $id, 'post_status' => 'inherit', 'post_type' => 'attachment', 'post_mime_type' => 'image', 'order' => $order, 'orderby' => $orderby ) );
    }

    if ( empty( $attachments ) ) return '';

    // Filter gallery differently for feeds
    if ( is_feed() ) {
        $output = "\n";
        foreach ( $attachments as $att_id => $attachment ) $output .= wp_get_attachment_link( $att_id, $size, true ) . "\n";
        return $output;
    }

    // Filter tags and attributes
    $itemtag = tag_escape( $itemtag );
    $captiontag = tag_escape( $captiontag );
    $columns = intval( $columns );
    $itemwidth = $columns > 0 ? floor( 100 / $columns ) : 100;
    $float = is_rtl() ? 'right' : 'left';
    $selector = "galeria--{$instance}";


    // Iterate through the attachments in this gallery instance
    $i = 0;
    $output .= '<div class="galeria">';

    foreach ( $attachments as $id => $attachment ) {

        // Attachment link
        $link = isset( $attr['link'] ) && 'file' == $attr['link'] ? wp_get_attachment_link( $id, $size, false, false ) : wp_get_attachment_link( $id, $size, true, false );

        // Start itemtag
        $output .= "<{$itemtag} class='galeria__item'>";

        // icontag
        $output .= "
		<{$icontag} class='galeria__icono'>
			$link
		";

        if ( $captiontag && trim( $attachment->post_excerpt ) ) {

            // captiontag
            $output .= "
    <{$captiontag} class='galeria__caption'>
    " . wptexturize($attachment->post_excerpt) . "
</{$captiontag}>
</{$icontag}>";

        }

        // End itemtag
        $output .= "</{$itemtag}>";

//

    }

    // End gallery output
    $output .= "</div>";

    return $output;

}

// Apply filter to default gallery shortcode
add_filter( 'post_gallery', 'my_post_gallery', 10, 2 );

