<?php
/**
 * The template for displaying all single posts.
 *
 * @package Portfolio
 */

get_header(); ?>
<div class="portada__contenedor">
    <?php $portada =  wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'large');?>
    <div class="portada__imagen" style="background-image: url('<?php echo $portada['0'];?>')">
     <div class="portada__contenedor-interno">
         <div class="portada__datos">
             <h1 class="portada__titulo">Hola esto es el titulo</h1>
         </div>
     </div>
    </div>
</div>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

		<?php while ( have_posts() ) : the_post(); ?>
			<?php
			$terms = wp_get_object_terms( $post->ID,  'tecnologia' );
			if ($terms) {
				echo '<h2>Tecnolog&iacute;a</h2>';
				foreach ($terms as $tec) {
					$link = get_term_link($tec);
					$nombre = $tec->name;
					echo '<a href="' . $link . '">' . $nombre . '</a> </br>';
				};
			}
			?>

			<?php
			$terms = wp_get_object_terms( $post->ID,  'cliente' );
			if ($terms) {
				echo '<h2>Cliente</h2>';
				foreach ($terms as $cliente) {
					$link = get_term_link($cliente);
					$nombre = $cliente->name;
					echo '<a href="' . $link . '">' . $nombre . '</a> </br>';
				};
			};
			?>

			<?php
			$terms = wp_get_object_terms( $post->ID,  'servicios' );
			if ($terms) {
				echo '<h2>Servicios</h2>';
				foreach ($terms as $servicios) {
					$link = get_term_link($servicios);
					$nombre = $servicios->name;
					echo '<a href="' . $link . '">' . $nombre . '</a> </br>';
				};
			}
			?>

			<?php
			$terms = wp_get_object_terms( $post->ID,  'colaborador' );
			if ($terms) {
				echo '<h2>Colaboradores</h2>';
				foreach ($terms as $colaboradores) {
					$link =  $colaboradores->description;
					$nombre = $colaboradores->name;
					echo '<a href="' . $link . '">' . $nombre . '</a> </br>';
				};
			}
			?>

			<?php get_template_part( 'template-parts/content', 'single' ); ?>

			<?php the_post_navigation(); ?>

			<?php
				// If comments are open or we have at least one comment, load up the comment template.
				if ( comments_open() || get_comments_number() ) :
					comments_template();
				endif;
			?>

		<?php endwhile; // End of the loop. ?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php get_footer(); ?>
